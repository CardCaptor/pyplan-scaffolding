import { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios'

import { Api } from '../common/api/api'
import { apiConfig } from '../common/api/api.config'

import { User } from '../common/types/user'
import { Credentials, Token } from '../common/types/security'

export class UserApi extends Api {
  public constructor(config: AxiosRequestConfig) {
    super(config)

    // this middleware is been called right before the http request is made.
    this.interceptors.request.use((param: AxiosRequestConfig) => ({
      ...param
    }))

    // this middleware is been called right before the response is get it by the method that triggers the request
    this.interceptors.response.use((param: AxiosResponse) => ({
      ...param
    }))

    Object.setPrototypeOf(this, UserApi.prototype)
    this.userLogin = this.userLogin.bind(this)
    this.userRegister = this.userRegister.bind(this)
    this.getAllUsers = this.getAllUsers.bind(this)
    this.getById = this.getById.bind(this)
  }

  public async userLogin(credentials: Credentials): Promise<Token> {
    const res: AxiosResponse<string> = await this.post<string, Credentials, AxiosResponse<string>>(
      '/rest-auth/',
      credentials
    )
    return this.success(res)
  }

  public userRegister(user: User): Promise<number> {
    return this.post<number, User, AxiosResponse<number>>('/users/register/', user)
      .then(this.success)
      .catch((error: AxiosError<Error>) => {
        throw error
      })
  }

  public async getAllUsers(): Promise<User[]> {
    try {
      const res: AxiosResponse<User[]> = await this.get<User, AxiosResponse<User[]>>('/users/')

      return this.success(res)
    } catch (error) {
      throw error
    }
  }

  public getById(id: number): Promise<User> {
    return this.get<User, AxiosResponse<User>>(`/users/${id}`).then(this.success)
  }
}

export const userApi = new UserApi(apiConfig)
