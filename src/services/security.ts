import { AxiosRequestConfig, AxiosResponse } from 'axios'

import { Api } from '../common/api/api'
import { apiConfig } from '../common/api/api.config'
import { TOKEN, SESSION_KEY } from '../common/api/localStorageConsts'

class SecurityApi extends Api {
  public constructor(config: AxiosRequestConfig) {
    super(config)

    // this middleware is been called right before the http request is made.
    this.interceptors.request.use((param: AxiosRequestConfig) => ({
      ...param
    }))

    // this middleware is been called right bjsonplaceholder.typicode.comefore the response is get it by the method that triggers the request
    this.interceptors.response.use((param: AxiosResponse) => ({
      ...param
    }))

    Object.setPrototypeOf(this, SecurityApi.prototype)
    this.loggedIn = this.loggedIn.bind(this)
    this.logIn = this.logIn.bind(this)
    this.createSession = this.createSession.bind(this)
    this.logOut = this.logOut.bind(this)
  }

  public async loggedIn(): Promise<boolean> {
    const cleanLocalStorage = () => {
      localStorage.removeItem(TOKEN)
      localStorage.removeItem(SESSION_KEY)
    }
    if (this.getToken()) {
      const res: AxiosResponse<boolean> = await this.get<string, AxiosResponse<boolean>>(
        '/security/isValid/'
      )
      if (res.status === 200 || res.statusText === 'OK') {
        return true
      }
      cleanLocalStorage()
      return false
    }
    cleanLocalStorage()
    return false
  }

  public async logIn({ username, password }: { username: string; password: string }): Promise<any> {
    const res: AxiosResponse<any> = await this.post<
      string,
      { username: string; password: string },
      AxiosResponse<any>
    >('/token-auth/', { username, password })
    return this.success(res)
  }

  public async createSession(companyId: number = -1): Promise<object> {
    const res: AxiosResponse<any> = await this.post<
      string,
      { companyId: number },
      AxiosResponse<any>
    >('/security/createSession', { companyId })
    return this.success(res)
  }

  public async logOut(): Promise<any> {
    const res: AxiosResponse<void> = await this.get<string, AxiosResponse<void>>(
      '/security/logout/'
    )
    return this.success(res)
  }
}

export const securityApi = new SecurityApi(apiConfig)
