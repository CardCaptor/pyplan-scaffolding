/**
 * SharedPage
 */

import { FC } from 'react'

import CssBaseline from '@material-ui/core/CssBaseline'

const SharedPage: FC = () => {
  return (
    <>
      <CssBaseline />
      <h4>Shared Page</h4>
    </>
  )
}

export default SharedPage
