/*
 * TestPage Messages
 *
 * This contains all the text for the TestPage component.
 */

import { defineMessages } from 'react-intl'

export default defineMessages({
  header: {
    id: 'app.components.TestPage.header',
    defaultMessage: 'This is TestPage component!'
  }
})
