import React from 'react'
import { FormattedMessage } from 'react-intl'
import { shallow } from '../../home/tests/node_modules/enzyme'

import TestPage from '../index'
import messages from '../messages'

describe('<TestPage />', () => {
  it('should render the page message', () => {
    const renderedComponent = shallow(<TestPage />)
    expect(renderedComponent.contains(<FormattedMessage {...messages.header} />)).toEqual(true)
  })
})
