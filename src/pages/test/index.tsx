import React from 'react'
import { FormattedMessage } from 'react-intl'

import messages from './messages'

const Test: React.FC = () => (
  <h1>
    <FormattedMessage {...messages.header} />
  </h1>
)

export default Test
