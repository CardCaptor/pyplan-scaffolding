/**
 * AppPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 */

import React, { lazy, useEffect } from 'react'
import { Link, Route, Switch, useHistory } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'

import { makeStyles, Theme, createStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import CssBaseline from '@material-ui/core/CssBaseline'
import useScrollTrigger from '@material-ui/core/useScrollTrigger'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Container from '@material-ui/core/Container'
import IconButton from '@material-ui/core/IconButton'
import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'
import Badge from '@material-ui/core/Badge'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEllipsisH, faEnvelope, faUserCircle, faBell } from '@fortawesome/pro-solid-svg-icons'

import { ApplicationState } from '../../store'
import { logout } from '../../store/user/actions'
import Notifier from './components/Notifier'

import pyplanLogoFull from '../../assets/img/pyplan-logo-full_196-50.png'
import pyplanLogoOnlySymbol from '../../assets/img/pyplan-logo-only-symbol.png'

const HomePage = lazy(() => import('../home/index'))
const TestPage = lazy(() => import('../test/index'))

interface Props {
  children: React.ReactElement
}

const ElevationScroll = (props: Props) => {
  const { children } = props
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0
  })
  return React.cloneElement(children, {
    elevation: trigger ? 4 : 0
  })
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    grow: {
      flexGrow: 1
    },
    menuButton: {
      marginRight: theme.spacing(2)
    },
    title: {
      display: 'none',
      [theme.breakpoints.up('sm')]: {
        display: 'block'
      }
    },
    inputRoot: {
      color: 'inherit'
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: '20ch'
      }
    },
    sectionDesktop: {
      display: 'none',
      [theme.breakpoints.up('md')]: {
        display: 'flex'
      }
    },
    sectionMobile: {
      display: 'flex',
      [theme.breakpoints.up('md')]: {
        display: 'none'
      }
    },
    logoMobile: {
      display: 'flex',
      [theme.breakpoints.down('md')]: {
        height: '5ch'
      }
    }
  })
)

const AppPage: React.FC<Props> = (props: Props) => {
  const classes = useStyles()
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const [anchorToolsMenuEl, setAnchorToolsMenuEl] = React.useState<null | HTMLElement>(null)
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState<null | HTMLElement>(null)
  const dispatch = useDispatch()
  const history = useHistory()

  const notLoggedIn: boolean = useSelector((state: ApplicationState) => !state.user.authData)
  useEffect(() => {
    // Checks if authData is stored in redux store, if not, it will redirect to auth
    if (notLoggedIn) {
      history.push('/auth/')
    }
  }, [history, notLoggedIn])

  const isMenuOpen = Boolean(anchorEl)
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl)
  const isMenuToolsOpen = Boolean(anchorToolsMenuEl)

  const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }
  const handleToolsMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorToolsMenuEl(event.currentTarget)
  }
  const handleMobileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setMobileMoreAnchorEl(event.currentTarget)
  }
  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null)
  }

  const handleMenuToolsClose = () => {
    setAnchorToolsMenuEl(null)
    // handleMobileMenuToolsClose()
  }
  const handleMenuClose = () => {
    setAnchorEl(null)
    handleMobileMenuClose()
  }
  const handleLogout = () => {
    dispatch(logout())
    handleMenuClose()
  }

  const menuId = 'primary-search-account-menu'
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleMenuClose}>Profile</MenuItem>
      <MenuItem onClick={handleMenuClose}>My account</MenuItem>
      <MenuItem onClick={handleLogout}>Logout</MenuItem>
    </Menu>
  )

  const menuToolsId = 'primary-tools-menu'
  const renderToolsMenu = (
    <Menu
      anchorEl={anchorToolsMenuEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={menuToolsId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuToolsOpen}
      onClose={handleMenuToolsClose}
    >
      <MenuItem onClick={handleMenuToolsClose} component={Link} to=''>
        Home Page
      </MenuItem>
      <MenuItem onClick={handleMenuToolsClose} component={Link} to='/test/'>
        Test Page
      </MenuItem>
    </Menu>
  )

  const mobileMenuId = 'primary-search-account-menu-mobile'
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <IconButton aria-label='show 4 new mails' color='inherit'>
          <Badge badgeContent={4} color='secondary'>
            <FontAwesomeIcon icon={faEnvelope} />
          </Badge>
        </IconButton>
        <p>Messages</p>
      </MenuItem>
      <MenuItem>
        <IconButton aria-label='show 11 new notifications' color='inherit'>
          <Badge badgeContent={11} color='secondary'>
            <FontAwesomeIcon icon={faBell} />
          </Badge>
        </IconButton>
        <p>Notifications</p>
      </MenuItem>
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          aria-label='account of current user'
          aria-controls='primary-search-account-menu'
          aria-haspopup='true'
          color='inherit'
        >
          <FontAwesomeIcon icon={faUserCircle} />
        </IconButton>
        <p>Profile</p>
      </MenuItem>
    </Menu>
  )

  return (
    <>
      <CssBaseline />
      <ElevationScroll {...props}>
        <AppBar color='secondary'>
          {/* >>>>>>>> DESKTOP <<<<<<<< */}
          <Toolbar className={classes.sectionDesktop}>
            <Link to='/'>
              <img src={pyplanLogoFull} className='App-logo' alt='logo' />
            </Link>
            <Typography className={classes.title} variant='caption' noWrap>
              V2.3.0
            </Typography>
            <div className={classes.grow} />
            <Button aria-controls={menuToolsId} onClick={handleToolsMenuOpen} color='inherit'>
              Tools
            </Button>
            <IconButton aria-label='show 4 new mails' color='inherit'>
              <Badge badgeContent={4} color='secondary'>
                <FontAwesomeIcon icon={faEnvelope} />
              </Badge>
            </IconButton>
            <IconButton aria-label='show 17 new notifications' color='inherit'>
              <Badge badgeContent={17} color='secondary'>
                <FontAwesomeIcon icon={faBell} />
              </Badge>
            </IconButton>
            <IconButton
              edge='end'
              aria-label='account of current user'
              aria-controls={menuId}
              aria-haspopup='true'
              onClick={handleProfileMenuOpen}
              color='inherit'
            >
              <FontAwesomeIcon icon={faUserCircle} />
            </IconButton>
          </Toolbar>
          {/* >>>>>>>> MOBILE <<<<<<<< */}
          <Toolbar className={classes.sectionMobile}>
            <img src={pyplanLogoOnlySymbol} className={classes.logoMobile} alt='logo' />
            <Typography className={classes.title} variant='h6' noWrap>
              V2.3.0
            </Typography>
            <div className={classes.grow} />
            <IconButton
              aria-label='show more'
              aria-controls={mobileMenuId}
              aria-haspopup='true'
              onClick={handleMobileMenuOpen}
              color='inherit'
            >
              <FontAwesomeIcon icon={faEllipsisH} />
            </IconButton>
          </Toolbar>
        </AppBar>
      </ElevationScroll>
      <Toolbar />
      {renderMobileMenu}
      {renderMenu}
      {renderToolsMenu}
      <Container>
        <Box my={2}>
          <Switch>
            <Route exact path='/' component={HomePage} />
            <Route exact path='/test/' component={TestPage} />
          </Switch>
        </Box>
      </Container>
      <Notifier />
    </>
  )
}

export default AppPage
