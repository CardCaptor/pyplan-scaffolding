import React from 'react'
import { FormattedMessage } from 'react-intl'

import messages from './messages'

const Home: React.FC = () => (
  <h1>
    <FormattedMessage {...messages.header} />
  </h1>
)

export default Home
