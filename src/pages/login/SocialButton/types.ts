import { MouseEvent } from 'react'

interface Message {
  id: string
  defaultMessage: string
}

export interface SocialButtonProps {
  app: string
  message: Message
  onClick: (event: MouseEvent<HTMLButtonElement>, app: string) => void
}
