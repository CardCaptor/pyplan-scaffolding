import { FC } from 'react'
import { FormattedMessage } from 'react-intl'

import { makeStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Avatar from '@material-ui/core/Avatar'
import Grid from '@material-ui/core/Grid'

import { SocialButtonProps } from './types'

import linkedinLogo from '../../../assets/img/pages/login/linkedin-logo.png'
import googleLogo from '../../../assets/img/pages/login/google-logo.png'
import officeLogo from '../../../assets/img/pages/login/office-logo.png'

const useStyles = makeStyles((theme) => ({
  linkedin: {
    color: '#fff',
    backgroundColor: '#0078BD',
    '&:hover': {
      backgroundColor: '#004d8c'
    }
  },
  google: {
    color: '#0078BD',
    backgroundColor: '#fff',
    '&:hover': {
      backgroundColor: '#cccccc'
    }
  },
  office: {
    color: '#fff',
    backgroundColor: '#FF0D15',
    '&:hover': {
      backgroundColor: '#c20000'
    }
  },
  avatar: {
    height: '20px',
    width: ' 20px'
  }
}))

const SocialButton: FC<SocialButtonProps> = ({ app, message, onClick }) => {
  const classes = useStyles()
  const getButtonProps = () => {
    switch (app) {
      case 'linkedin':
        return {
          text: 'Linkedin',
          className: classes.linkedin,
          logo: (
            <Avatar className={classes.avatar} variant='square' alt='Linkedin' src={linkedinLogo} />
          )
        }
      case 'google':
        return {
          text: 'Google',
          className: classes.google,
          logo: <Avatar className={classes.avatar} variant='square' alt='Google' src={googleLogo} />
        }
      case 'office':
        return {
          text: 'Office 365',
          className: classes.office,
          logo: (
            <Avatar className={classes.avatar} variant='square' alt='Office 365' src={officeLogo} />
          )
        }
    }
  }

  const buttonProps = getButtonProps()

  return (
    <Button className={buttonProps?.className} size='small' onClick={(e) => onClick(e, app)}>
      <Grid container alignItems='center' justify='center' spacing={1}>
        <Grid item>{buttonProps?.logo}</Grid>
        <Grid item>
          <FormattedMessage {...message} values={{ app: buttonProps?.text }} />
        </Grid>
      </Grid>
    </Button>
  )
}

export default SocialButton
