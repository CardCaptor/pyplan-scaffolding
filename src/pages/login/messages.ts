import { defineMessages } from 'react-intl'

export default defineMessages({
  init: {
    id: 'app.pages.login.init',
    defaultMessage: 'Init'
  },
  usernameLabel: {
    id: 'app.pages.login.usernameLabel',
    defaultMessage: 'Username'
  },
  passwordLabel: {
    id: 'app.pages.login.passwordLabel',
    defaultMessage: 'Password'
  },
  forgotPassword: {
    id: 'app.pages.login.forgotPassword',
    defaultMessage: 'Forgot password?'
  },
  login: {
    id: 'app.pages.login.login',
    defaultMessage: 'Login'
  },
  connectWithSocialMedia: {
    id: 'app.pages.login.connectWithSocialMedia',
    defaultMessage: 'Connect with social media'
  },
  or: {
    id: 'app.pages.login.or',
    defaultMessage: 'or by default'
  },
  connectWithApp: {
    id: 'app.pages.login.connectWithApp',
    defaultMessage: 'Connect with {app}'
  },
  createAnAccount: {
    id: 'app.pages.login.createAnAccount',
    defaultMessage: 'Create an account'
  },
  copyright: {
    id: 'app.pages.login.copyright',
    defaultMessage: 'Copyright © Pyplan {year}.'
  }
})
