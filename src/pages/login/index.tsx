import { ChangeEvent, FC, FormEvent, MouseEvent, useEffect, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'

// Material UI imports
import { Theme, makeStyles } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
import Hidden from '@material-ui/core/Hidden'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Link from '@material-ui/core/Link'
import Button from '@material-ui/core/Button'

// Custom imports
// intl
import messages from './messages'
// storage
import { login, selectCompany } from '../../store/user/actions'
import { ApplicationState } from '../../store'
// types
import { UserSessionData, UserAuthData } from '../../common/types/user'
//components and assets
import pyplanLogoFull from '../../assets/img/pyplan-logo-full_626-246.png'
import SocialButton from './SocialButton'
import CardWithLogo from '../../common/components/cards/CardWithLogo'

const useStyles = makeStyles((theme: Theme) => ({
  loginBackgroundPaper: {
    height: '100vh',
    backgroundColor: theme.palette.type === 'light' ? theme.palette.primary.main : '#1b1b1b',
    borderRadius: 0
  },
  containerLoginGrid: {
    minHeight: '100vh'
  },
  loginWrapperPaper: {
    paddingLeft: theme.spacing(6),
    paddingRight: theme.spacing(6)
  },
  textAlignCenter: {
    textAlign: 'center'
  },
  logo: {
    maxHeight: '100px',
    maxWidth: '100%'
  },
  sectionsText: {
    color: theme.palette.type === 'light' ? '#4B4B4B' : '#ccc', //TODO: ver si este color es muy especifico
    font: 'normal normal bold 16px/19px Roboto',
    letterSpacing: '0px'
  },
  form: {
    width: '100%' // Fix IE 11 issue.
  },
  passwordTextField: {
    marginBottom: 0
  },
  forgotPasswordLink: {
    color: theme.palette.type === 'light' ? '#014F98' : '#ccc',
    font: 'normal normal 300 12px/14px Roboto',
    letterSpacing: '0px'
  },
  orText: {
    color: theme.palette.type === 'light' ? '#4B4B4B' : '#ccc', //TODO: ver si este color es muy especifico
    font: 'normal normal bold 12px/14px Roboto',
    letterSpacing: '0px'
  }
}))

const WITH_SOCIAL_LOGIN = true //TODO: obetener esto desde una config

const LoginPage: FC = () => {
  const [username, setUsername] = useState<string>('')
  const [password, setPassword] = useState<string>('')

  const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    const {
      target: { name, value }
    } = e
    switch (name) {
      case 'username':
        setUsername(value)
        break
      case 'password':
        setPassword(value)
        break
    }
  }

  const handleSocialButtonClick = (event: MouseEvent<HTMLButtonElement>, app: string) => {
    console.log(`click from ${app}`)
  }

  const handleCreateAccountClick = (event: MouseEvent<HTMLButtonElement>) => {
    console.log('click from create account')
  }

  const handleSelectCompany = (id: number) => {
    dispatch(selectCompany({ selectedCompanyId: id }))
  }

  const dispatch = useDispatch()
  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    dispatch(login({ username, password }))
  }

  const history = useHistory()
  const authData: UserAuthData | undefined = useSelector(
    (state: ApplicationState) => state.user.authData
  )
  const sessionData: UserSessionData | undefined = useSelector(
    (state: ApplicationState) => state.user.sessionData
  )
  const selectedCompanyId: number | undefined = useSelector(
    (state: ApplicationState) => state.user.selectedCompanyId
  )
  useEffect(() => {
    if (sessionData?.session_key) {
      history.push('/')
    }
  }, [history, sessionData])

  const classes = useStyles()
  return (
    <Paper className={classes.loginBackgroundPaper}>
      <CssBaseline />
      <Grid
        container
        direction='row'
        justify='center'
        alignItems='center'
        className={classes.containerLoginGrid}
      >
        <Hidden xsDown>
          <Grid item md={WITH_SOCIAL_LOGIN ? 3 : 4}></Grid>
        </Hidden>
        <Grid item xs={12} md={WITH_SOCIAL_LOGIN ? 6 : 4}>
          <Paper elevation={3} className={classes.loginWrapperPaper}>
            <Grid item container direction='row' justify='center' alignItems='center'>
              <Grid item xs={12} className={classes.textAlignCenter}>
                <img src={pyplanLogoFull} className={classes.logo} alt='logo' />
              </Grid>
            </Grid>
            {!authData ? (
              <form className={classes.form} noValidate onSubmit={handleSubmit}>
                {WITH_SOCIAL_LOGIN ? (
                  <Grid
                    item
                    container
                    direction='row'
                    justify='space-around'
                    alignItems='stretch'
                    spacing={6}
                  >
                    {/** controls the space between sections */}
                    <Grid
                      item
                      xs={12}
                      md={6}
                      container
                      spacing={2}
                      direction='column'
                      justify='flex-start'
                      alignItems='stretch'
                    >
                      {/**Section init (controls space between rows inside the section) */}
                      <Grid item>
                        <Typography component='h1' className={classes.sectionsText}>
                          <FormattedMessage {...messages.init} />
                        </Typography>
                      </Grid>
                      <Grid item>
                        <TextField
                          required
                          id='username'
                          label={<FormattedMessage {...messages.usernameLabel} />}
                          name='username'
                          autoFocus
                          value={username}
                          onChange={handleInputChange}
                          size='small'
                        />
                      </Grid>
                      <Grid item>
                        <>
                          <TextField
                            required
                            name='password'
                            label={<FormattedMessage {...messages.passwordLabel} />}
                            type='password'
                            id='password'
                            value={password}
                            onChange={handleInputChange}
                            className={classes.passwordTextField}
                            size='small'
                          />
                          <div className={classes.textAlignCenter}>
                            <Link href='#' className={classes.forgotPasswordLink}>
                              <FormattedMessage {...messages.forgotPassword} />
                            </Link>
                          </div>
                        </>
                      </Grid>
                      <Grid item>
                        <Button type='submit' color='primary' size='small'>
                          <FormattedMessage {...messages.login} />
                        </Button>
                      </Grid>
                    </Grid>
                    {/**Section social (controls space between rows inside the section) */}
                    <Grid
                      item
                      container
                      xs={12}
                      md={6}
                      spacing={3}
                      direction='column'
                      justify='flex-start'
                      alignItems='stretch'
                    >
                      <Grid item>
                        <Typography component='h1' className={classes.sectionsText}>
                          <FormattedMessage {...messages.connectWithSocialMedia} />
                        </Typography>
                      </Grid>
                      <Grid item>
                        <SocialButton
                          app='linkedin'
                          message={messages.connectWithApp}
                          onClick={handleSocialButtonClick}
                        />
                      </Grid>
                      <Grid item>
                        <SocialButton
                          app='google'
                          message={messages.connectWithApp}
                          onClick={handleSocialButtonClick}
                        />
                      </Grid>
                      <Grid item className={classes.textAlignCenter}>
                        <Typography className={classes.orText}>
                          <FormattedMessage {...messages.or} />
                        </Typography>
                      </Grid>
                      <Grid item>
                        <Button
                          variant='outlined'
                          color='primary'
                          size='small'
                          onClick={handleCreateAccountClick}
                        >
                          <FormattedMessage {...messages.createAnAccount} />
                        </Button>
                      </Grid>
                    </Grid>
                  </Grid>
                ) : (
                  <Grid
                    item
                    xs={12}
                    container
                    spacing={2}
                    direction='column'
                    justify='flex-start'
                    alignItems='stretch'
                  >
                    <Grid item>
                      <Typography component='h1' className={classes.sectionsText}>
                        <FormattedMessage {...messages.init} />
                      </Typography>
                    </Grid>
                    <Grid item>
                      <TextField
                        required
                        id='username'
                        label={<FormattedMessage {...messages.usernameLabel} />}
                        name='username'
                        autoFocus
                        value={username}
                        onChange={handleInputChange}
                        size='small'
                      />
                    </Grid>
                    <Grid item>
                      <>
                        <TextField
                          required
                          name='password'
                          label={<FormattedMessage {...messages.passwordLabel} />}
                          type='password'
                          id='password'
                          value={password}
                          onChange={handleInputChange}
                          className={classes.passwordTextField}
                          size='small'
                        />
                        <div className={classes.textAlignCenter}>
                          <Link href='#' className={classes.forgotPasswordLink}>
                            <FormattedMessage {...messages.forgotPassword} />
                          </Link>
                        </div>
                      </>
                    </Grid>
                    <Grid item>
                      <Button type='submit' color='primary' size='small'>
                        <FormattedMessage {...messages.login} />
                      </Button>
                    </Grid>
                  </Grid>
                )}
              </form>
            ) : (
              <Grid
                item
                container
                direction='row'
                justify='space-around'
                alignItems='stretch'
                spacing={3}
              >
                {authData.companies.map((company) => (
                  <Grid item xs={12} md={6} key={company.id}>
                    <CardWithLogo
                      id={company.id}
                      name={company.name}
                      image={company.image_url}
                      selected={company.id === selectedCompanyId}
                      onClick={(e: MouseEvent<HTMLDivElement>, id: number) =>
                        handleSelectCompany(id)
                      }
                    />
                  </Grid>
                ))}
                {authData && authData?.companies.length % 2 !== 0 && (
                  <Grid item xs={12} md={6}></Grid>
                )}
              </Grid>
            )}
            <Grid item xs={12} container spacing={5} direction='column'>
              <Grid item>
                <Link color='inherit' href='https://pyplan.com/'>
                  <Typography variant='body2' color='textSecondary' align='center'>
                    <FormattedMessage
                      {...messages.copyright}
                      values={{ year: new Date().getFullYear() }}
                    />
                  </Typography>
                </Link>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
        <Hidden xsDown>
          <Grid item md={WITH_SOCIAL_LOGIN ? 3 : 4}></Grid>
        </Hidden>
      </Grid>
    </Paper>
  )
}
export default LoginPage
