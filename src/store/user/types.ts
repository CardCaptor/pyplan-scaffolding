import { UserAuthData, UserSessionData } from '../../common/types/user'

// Use enums for better autocompletion of action type names. These will
// be compiled away leaving only the final value in your compiled code.
//
// Define however naming conventions you'd like for your action types, but
// personally, I use the `@@context/ACTION_TYPE` convention, to follow the convention
// of Redux's `@@INIT` action.
export enum UserActionTypes {
  LOGIN = '@@user/LOGIN',
  LOGOUT = '@@user/LOGOUT',
  SELECT_COMPANY = '@@user/SELECT_COMPANY',
  SET_USER_AUTH_DATA = '@@user/SET_USER_AUTH_DATA',
  SET_USER_SESSION_DATA = '@@user/SET_USER_SESSION_DATA',
  SET_SELECTED_COMPANY_ID = '@@user/SET_SELECTED_COMPANY_ID',
  SET_USER = '@@user/SET_USER',
  GET_USER = '@@user/GET_USER',
  FETCH_REQUEST = '@@user/FETCH_REQUEST',
  FETCH_SUCCESS = '@@user/FETCH_SUCCESS',
  FETCH_ERROR = '@@user/FETCH_ERROR',
  SELECT_USER = '@@user/SELECT_USER',
  SELECTED = '@@user/SELECTED'
}

// Declare state types with `readonly` modifier to get compile time immutability.
// https://github.com/piotrwitek/react-redux-typescript-guide#state-with-type-level-immutability
export interface UserState {
  readonly authData?: UserAuthData
  readonly sessionData?: UserSessionData
  readonly selectedCompanyId?: number | undefined
}
