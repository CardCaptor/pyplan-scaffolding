import { Reducer } from 'redux'
import { UserState, UserActionTypes } from './types'

export const initialState: UserState = {
  authData: undefined,
  sessionData: undefined,
  selectedCompanyId: undefined
}

// Thanks to Redux 4's much simpler typings, we can take away a lot of typings on the reducer side,
// everything will remain type-safe.
const reducer: Reducer<UserState> = (
  state: UserState = initialState,
  action: { type: UserActionTypes; payload?: any }
) => {
  switch (action.type) {
    case UserActionTypes.SET_USER_AUTH_DATA: {
      return {
        ...state,
        ...{ authData: action.payload }
      }
    }
    case UserActionTypes.SET_USER_SESSION_DATA: {
      return {
        ...state,
        ...{ sessionData: action.payload }
      }
    }
    case UserActionTypes.SET_SELECTED_COMPANY_ID: {
      return {
        ...state,
        ...{ selectedCompanyId: action.payload }
      }
    }
    default: {
      return state
    }
  }
}

// Instead of using default export, we use named exports. That way we can group these exports
// inside the `index.js` folder.
export { reducer as userReducer }
