import { all, call, fork, put, race, take, takeEvery, takeLatest } from 'redux-saga/effects'
import { AnyAction } from 'redux'

import {
  fetchError,
  fetchSuccess,
  setUserAuthData,
  setSelectedCompanyId,
  setUserSessionData
} from './actions'
import { userApi } from '../../services/user'
import { securityApi } from '../../services/security'
import { User } from '../../common/types/user'
import { UserActionTypes } from './types'
import enqueue from '../../common/notifier'
import { TOKEN, SESSION_KEY, LAST_COMPANY_SELECTED } from '../../common/api/localStorageConsts'

export function* authorize({ username, password }: { username: string; password: string }) {
  try {
    // yield put(sendingRequest(true))
    return yield call(securityApi.logIn, { username, password })
  } catch (error) {
    yield put(enqueue(error))
  } finally {
    // yield put(sendingRequest(false))
  }
}

function* handleLogin({ payload: { username, password } }: AnyAction) {
  const winner = yield race({
    auth: call(authorize, { username, password }),
    logout: take(UserActionTypes.LOGOUT)
  })
  if (winner?.auth?.companies) {
    // Save token to local storage
    localStorage[TOKEN] = winner.auth.token
    yield put(setUserAuthData(winner.auth))
    // yield put(setAuthState(true))
    if (winner.auth.companies.length === 1) {
      const session = yield call(securityApi.createSession, winner.auth.companies[0].id)
      localStorage[SESSION_KEY] = session.session_key
      yield put(setUserSessionData(session))
    } else {
      const selectedCompanyId =
        localStorage.getItem(LAST_COMPANY_SELECTED) || winner.auth.companies[0].id
      yield put(setSelectedCompanyId(parseInt(selectedCompanyId)))
    }
  }
}

function* handleSelectCompany({ payload: { selectedCompanyId } }: AnyAction) {
  localStorage[LAST_COMPANY_SELECTED] = selectedCompanyId
  yield put(setSelectedCompanyId(selectedCompanyId))
  const session = yield call(securityApi.createSession, selectedCompanyId)
  localStorage[SESSION_KEY] = session.session_key
  yield put(setUserSessionData(session))
}

export function* handleLogout() {
  try {
    // yield put(sendingRequest(true))
    yield call(securityApi.logOut)
    // yield put(setAuthState(false))
    localStorage.removeItem(TOKEN)
    localStorage.removeItem(SESSION_KEY)
    yield put(setUserAuthData(null))
    yield put(setUserSessionData(null))
    return true
  } catch (error) {
    yield put(enqueue(error))
  } finally {
    // yield put(sendingRequest(false))
  }
}

function* handleFetch() {
  try {
    // To call async functions, use redux-saga's `call()`.
    const res: User[] = yield call(userApi.getAllUsers)

    // if (res.error) {
    //   yield put(fetchError(res.error))
    // } else {
    yield put(fetchSuccess(res))
  } catch (err) {
    if (err instanceof Error && err.stack) {
      yield put(fetchError(err.stack))
    } else {
      yield put(fetchError('An unknown error occured.'))
    }
  }
}

// This is our watcher function. We use `take*()` functions to watch Redux for a specific action
// type, and run our saga, for example the `handleFetch()` saga above.
function* watchSecurityActions() {
  yield all([
    yield takeEvery(UserActionTypes.LOGIN, handleLogin),
    yield takeEvery(UserActionTypes.SELECT_COMPANY, handleSelectCompany),
    yield takeLatest(UserActionTypes.LOGOUT, handleLogout)
  ])
}

function* watchFetchRequest() {
  yield takeEvery(UserActionTypes.FETCH_REQUEST, handleFetch)
}

// We can also use `fork()` here to split our saga into multiple watchers.
function* usersSaga() {
  yield all([fork(watchSecurityActions), fork(watchFetchRequest)])
}

export { usersSaga }
