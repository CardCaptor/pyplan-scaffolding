import { User, UserAuthData, UserSessionData } from '../../common/types/user'
import { UserActionTypes } from './types'

// Remember, you can also pass parameters into an action creator. Make sure to
// type them properly as well.

/**
 * login
 * @param {string} username
 * @param {string} password
 * @return {object}
 */
export const login = ({ username, password }: { username: string; password: string }): object => {
  return {
    type: UserActionTypes.LOGIN,
    payload: { username, password }
  }
}

/**
 * logout
 * @return {object}
 */
export const logout = (): object => {
  return {
    type: UserActionTypes.LOGOUT
  }
}

export const selectCompany = ({ selectedCompanyId }: { selectedCompanyId: number }): object => {
  return {
    type: UserActionTypes.SELECT_COMPANY,
    payload: { selectedCompanyId }
  }
}

export const setUserAuthData = (data: UserAuthData | null) => {
  return {
    type: UserActionTypes.SET_USER_AUTH_DATA,
    payload: data
  }
}

export const setSelectedCompanyId = (selectedCompanyId: number | undefined) => {
  return {
    type: UserActionTypes.SET_SELECTED_COMPANY_ID,
    payload: selectedCompanyId
  }
}

export const setUserSessionData = (data: UserSessionData | null) => {
  return {
    type: UserActionTypes.SET_USER_SESSION_DATA,
    payload: data
  }
}

export const setUser = (name: string, surname: string) => ({
  type: UserActionTypes.SET_USER,
  payload: { name, surname }
})

export const fetchRequest = () => ({ type: UserActionTypes.FETCH_REQUEST })
// export const clearSelected = () => ({ type: UserActionTypes.CLEAR_SELECTED })

// Remember, you can also pass parameters into an action creator. Make sure to
// type them properly as well.
export const fetchSuccess = (data: User[]) => ({
  type: UserActionTypes.FETCH_SUCCESS,
  payload: data
})
export const fetchError = (message: string) => ({
  type: UserActionTypes.FETCH_ERROR,
  payload: message
})
// export const selectUser = (userId: string) => ({ type: UserActionTypes.SELECT_USER, payload: userId })
// export const userSelected = (user: UserSelectedPayload) => ({ type: UserActionTypes.SELECTED, payload: user })
