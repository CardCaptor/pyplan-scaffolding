import { OptionsObject } from 'notistack'

export enum NotifierActionTypes {
  ENQUEUE_NOTIFICATION = '@@notifier/ENQUEUE_NOTIFICATION',
  CLOSE_NOTIFICATION = '@@notifier/CLOSE_NOTIFICATION',
  REMOVE_NOTIFICATION = '@@notifier/REMOVE_NOTIFICATION'
}

export type Notification = {
  key: string
  message: string
  options: OptionsObject
  data?: any
  dismissed?: boolean
}

// Declare state types with `readonly` modifier to get compile time immutability.
// https://github.com/piotrwitek/react-redux-typescript-guide#state-with-type-level-immutability
export interface NotifierState {
  readonly notifications: Notification[]
}
