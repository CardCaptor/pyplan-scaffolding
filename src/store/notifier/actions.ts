import { NotifierActionTypes } from './types'

export const enqueueSnackbar = (notification: {
  message: string
  data?: any
  options: { key: any; variant?: string; persist: boolean; action?: any }
}) => {
  const key = notification.options && notification.options.key

  return {
    type: NotifierActionTypes.ENQUEUE_NOTIFICATION,
    notification: {
      ...notification,
      key: key || new Date().getTime() + Math.random()
    }
  }
}

export const closeSnackbar = (key: any) => ({
  type: NotifierActionTypes.CLOSE_NOTIFICATION,
  dismissAll: !key, // dismiss all if no key has been defined
  key
})

export const removeSnackbar = (key: any) => ({
  type: NotifierActionTypes.REMOVE_NOTIFICATION,
  key
})
