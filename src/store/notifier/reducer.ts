import { Reducer } from 'redux'
import { OptionsObject } from 'notistack'

import { NotifierActionTypes, NotifierState } from './types'

const initState: NotifierState = {
  notifications: []
}

type ActionType = {
  type: NotifierActionTypes
  key: string
  notification: {
    key: string
    message: string
    data?: any
    options: OptionsObject
  }
  dismissed?: boolean
  dismissAll?: boolean
}

const reducer: Reducer<NotifierState, ActionType> = (
  state: NotifierState = initState,
  action: ActionType
) => {
  switch (action.type) {
    case NotifierActionTypes.ENQUEUE_NOTIFICATION:
      return {
        ...state,
        notifications: [
          ...state.notifications,
          {
            ...action.notification,
            key: action.notification.key
          }
        ]
      }
    case NotifierActionTypes.CLOSE_NOTIFICATION:
      return {
        ...state,
        notifications: state.notifications.map((notification) =>
          action.dismissAll || notification.key === action.key
            ? { ...notification, dismissed: true }
            : { ...notification }
        )
      }
    case NotifierActionTypes.REMOVE_NOTIFICATION:
      return {
        ...state,
        notifications: state.notifications.filter((notification) => notification.key !== action.key)
      }
    default:
      return state
  }
}

// Instead of using default export, we use named exports. That way we can group these exports
// inside the `index.js` folder.
export { reducer as notifierReducer }
