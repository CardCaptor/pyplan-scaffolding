import { LayoutActionTypes, ThemeColors } from './types'

// Remember, you can also pass parameters into an action creator. Make sure to
// type them properly as well.

export const setTheme = (theme: ThemeColors) => ({ type: LayoutActionTypes.SET_THEME, theme })
