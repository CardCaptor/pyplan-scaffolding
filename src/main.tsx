import * as React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter as Router } from 'react-router-dom'
import { Store } from 'redux'
import { IntlProvider } from 'react-intl'
import { SnackbarProvider } from 'notistack'
import { ThemeProvider } from '@material-ui/core/styles'

import Routes from './common/router/routes'
import { LOCALE } from './common/api/localStorageConsts'
import { ApplicationState } from './store'
import messages from './common/lang'
import * as themes from './common/theme'
import LayoutContainer from './common/containers/LayoutContainer'
import SuspenseWrapper from './common/components/SuspenseWrapper'

// Any additional component props go here.
interface MainProps {
  store: Store<ApplicationState>
}

const locale = (localStorage.getItem(LOCALE) as keyof typeof messages) || 'en-US'

// Create an intersection type of the component props and our Redux props.
const Main: React.FC<MainProps> = ({ store }) => {
  return (
    <Provider store={store}>
      <SnackbarProvider>
        <Router>
          <SuspenseWrapper>
            <IntlProvider messages={messages[locale]} locale={locale} defaultLocale='en-US'>
              <LayoutContainer>
                {({ theme }) => (
                  <ThemeProvider theme={themes[theme]}>
                    <Routes />
                  </ThemeProvider>
                )}
              </LayoutContainer>
            </IntlProvider>
          </SuspenseWrapper>
        </Router>
      </SnackbarProvider>
    </Provider>
  )
}

// Normally you wouldn't need any generics here (since types infer from the passed functions).
// But since we pass some props from the `index.js` file, we have to include them.
// For an example of a `connect` function without generics, see `./containers/LayoutContainer`.
export default Main
