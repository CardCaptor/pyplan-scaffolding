import React from 'react'
import { render } from 'react-dom'

import Main from './main'

import './assets/index.css'

import reportWebVitals from './reportWebVitals'

import configureStore from './configureStore'

const store = configureStore()

render(
  <React.StrictMode>
    <Main store={store} />
  </React.StrictMode>,
  document.getElementById('root')
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
