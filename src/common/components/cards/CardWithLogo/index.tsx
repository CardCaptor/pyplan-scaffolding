import { FC, useState } from 'react'
import { Theme, createStyles, makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'

import { CardWithLogoProps } from './types'
import defaultPyplanLogo from '../../../../assets/img/pyplan-logo-only-symbol.png'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card: {
      display: 'flex',
      backgroundColor: theme.palette.type === 'light' ? '#CEE8FF' : '#E1E2E1'
    },
    selected: {
      border: `1px solid ${theme.palette.primary.main}`
    },
    logo: {
      margin: theme.spacing(1),
      minWidth: 50,
      height: 50,
      borderRadius: '50%'
    },
    textContainer: {
      overflow: 'hidden',
      '&:last-child': {
        paddingTop: '24px'
      }
    },
    textWrapper: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'stretch',
      height: '100%'
    },
    text: {
      color: '#4B4B4B', //TODO: ver si este color es muy especifico
      font: 'normal normal bold 16px/18px Roboto',
      letterSpacing: '0px',
      overflowWrap: 'break-word'
    }
  })
)

const CardWithLogo: FC<CardWithLogoProps> = ({ id, name, image, selected, onClick }) => {
  const [raised, setRaised] = useState<boolean>(false)

  const classes = useStyles()

  return (
    <Card
      raised={raised}
      className={!selected ? classes.card : `${classes.card} ${classes.selected}`}
      onClick={(e) => onClick(e, id)}
      onMouseEnter={() => setRaised(true)}
      onMouseLeave={() => setRaised(false)}
    >
      <CardMedia
        className={classes.logo}
        image={image && image !== '' ? image : defaultPyplanLogo}
        title='Logo'
      />
      <CardContent className={classes.textContainer}>
        <div className={classes.textWrapper}>
          <Typography className={classes.text}>{name}</Typography>
        </div>
      </CardContent>
    </Card>
  )
}

export default CardWithLogo
