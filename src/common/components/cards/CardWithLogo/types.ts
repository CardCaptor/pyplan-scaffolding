import { MouseEvent } from 'react'

export interface CardWithLogoProps {
  id: number
  name: string
  image: string | undefined
  selected: boolean
  onClick: (event: MouseEvent<HTMLDivElement>, id: number) => void
}
