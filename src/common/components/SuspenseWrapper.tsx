import React, { Suspense, ReactElement } from 'react'

import LoadingIndicator from './layout/loading/index'
interface Props {
  children: ReactElement
}

const SuspenseWrapper: React.FC<Props> = ({ children }) => {
  return <Suspense fallback={<LoadingIndicator />}>{children}</Suspense>
}

export default SuspenseWrapper
