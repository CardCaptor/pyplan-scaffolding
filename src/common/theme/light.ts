/**
 * Material UI Theme Customization
 * Refer to: https://material-ui.com/customization/themes/
 */
import { createMuiTheme, ThemeOptions } from '@material-ui/core/styles'

const lightTheme: ThemeOptions = {
  palette: {
    type: 'light',
    common: {
      black: '#000',
      white: '#FFF'
    },
    primary: {
      main: '#368EE0'
    },
    secondary: {
      main: '#FFF',
      dark: '#368EE0',
      contrastText: '#368EE0',
      light: '#C3E2FE'
    },
    error: {
      main: '#FF0D15'
    }
  },
  typography: {
    fontFamily: ['Roboto', 'sans-serif'].join(','),
    fontSize: 16
  },
  //default component properties
  props: {
    MuiTextField: {
      fullWidth: true,
      variant: 'outlined',
      margin: 'normal'
    },
    MuiButton: {
      fullWidth: true,
      variant: 'contained'
    }
  },
  overrides: {
    MuiPaper: {
      rounded: { borderRadius: 5 }
    },
    // For TextField we need to edit the nested elements since TextField is only a wrapper
    // TextField prop size = 'small' its translated to marginDense in styles
    // Suppose we want a TextField to be 32px height (it's default size is 56px), we just have to do (56px - 32px) / 2 = 12px

    MuiOutlinedInput: {
      inputMarginDense: {
        // 6.5px = 18.5px - 12px (note: 18.5px is the input's default padding top and bottom)
        paddingTop: '6.5px',
        paddingBottom: '6.5px',
        font: 'normal normal normal 16px/19px Roboto',
        letterSpacing: '0px'
      }
    },
    MuiInputLabel: {
      outlined: {
        transform: 'translate(14px, 16px) scale(1)'
      },
      marginDense: {
        top: '-4px'
      }
    },
    MuiButton: {
      sizeSmall: {
        heigth: '32px'
      },
      label: {
        font: 'normal normal normal 16px/19px Roboto',
        letterSpacing: '0px',
        textTransform: 'none'
      }
    }
  }
}

export default createMuiTheme(lightTheme)
