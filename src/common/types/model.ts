export type ModelInfo = {
  modelId: string
  name: string
  new_session_key: string
  uri: string
  daysToExpire: string
  readonly: boolean
  readOnlyReason: string
  engineURI: string
  engineUID: string
  engineParams: string
  onOpenModel: string
  onOpenDashId: string
}
