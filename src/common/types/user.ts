import { Company } from './company'
import { ModelInfo } from './model'

// Response object for GET /users
export interface User extends ApiResponse {
  id: number
  name: string
  localized_name: string
  primary_attr: string
  attack_type: string
  roles: string[]
  img: string
  icon: string
}

// This type is basically shorthand for `{ [key: string]: any }`. Feel free to replace `any` with
// the expected return type of your API response.
export type ApiResponse = Record<string, any>

export type UserAuthData = {
  id: string
  token: string
  email: string
  companies: Company[]
}

export type UserSessionData = {
  session_key: string
  userId: string
  userFullName: string
  userFirstName: string
  userLastName: string
  userName: string
  userCompanyId: number
  userIsSuperUser: boolean
  userIsStaff: boolean
  userHome: string
  companyId: number
  company_code: string
  companyName: string
  process: string[]
  modelInfo: ModelInfo
  imageUrl: string
  loginModule: string
  autoopenUri: string
  decimalSep: string
  thousandSep: string
  copyDecimalSep: string
  companySettingFile: string
  loginAction: object
  created_at: string
  departments: string[]
  groups: string[]
  sessionTimeout: number
  sessionTimeoutForProcessing: number
  guestSessionTimeout: number
}
