export type Company = {
  id: number
  code: string
  name: string
  system: boolean
  active: boolean
  licence: string
  activation_code: string
  image_url?: string
}
