import { enqueueSnackbar } from '../../store/notifier'
import closeAction from './CloseBtnEnq'

const enqueue = (error: { toString: () => any }) => {
  const key = new Date().getTime() + Math.random()
  return enqueueSnackbar({
    message: error.toString(),
    data: error,
    options: {
      key,
      variant: 'warning',
      persist: true,
      action: () => closeAction(key)
    }
  })
}

export default enqueue
