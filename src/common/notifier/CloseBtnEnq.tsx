import { FC } from 'react'
import { useDispatch } from 'react-redux'

import Button from '@material-ui/core/Button'

import { closeSnackbar } from '../../store/notifier'

type Props = {
  key: string | number
}
const CloseBtnEnq: FC<Props> = ({ key }) => {
  const dispatch = useDispatch()
  const closeSnach = () => {
    dispatch(closeSnackbar(key))
  }
  return <Button onClick={closeSnach}>Dismiss</Button>
}

const closeAction = (key: string | number) => {
  return <CloseBtnEnq key={key} />
}

export default closeAction
