const messages = {
  //login
  'app.pages.login.init': 'Início',
  'app.pages.login.usernameLabel': 'Usuário',
  'app.pages.login.passwordLabel': 'Senha',
  'app.pages.login.forgotPassword': 'Esqueceu sua senha?',
  'app.pages.login.login': 'Iniciar Sessão',
  'app.pages.login.connectWithSocialMedia': 'Conecte-se com redes sociais',
  'app.pages.login.connectWithApp': 'Conectar com {app}',
  'app.pages.login.or': 'ou',
  'app.pages.login.createAnAccount': 'Criar uma conta',
  'app.pages.login.copyright': 'Copyright © Pyplan {year}.'
}

export default messages
