const messages = {
  //login
  'app.pages.login.init': 'Init',
  'app.pages.login.usernameLabel': 'Username',
  'app.pages.login.passwordLabel': 'Password',
  'app.pages.login.forgotPassword': 'Forgot password?',
  'app.pages.login.login': 'Log In',
  'app.pages.login.connectWithSocialMedia': 'Connect with social media',
  'app.pages.login.connectWithApp': 'Connect with {app}',
  'app.pages.login.or': 'or',
  'app.pages.login.createAnAccount': 'Create an account',
  'app.pages.login.copyright': 'Copyright © Pyplan {year}.'
}

export default messages
