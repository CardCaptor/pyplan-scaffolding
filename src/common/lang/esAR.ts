const messages = {
  //login
  'app.pages.login.init': 'Inicio',
  'app.pages.login.usernameLabel': 'Usuario',
  'app.pages.login.passwordLabel': 'Contraseña',
  'app.pages.login.forgotPassword': '¿Olvidó la contraseña?',
  'app.pages.login.login': 'Iniciar Sesión',
  'app.pages.login.connectWithSocialMedia': 'Conectar con redes sociales',
  'app.pages.login.connectWithApp': 'Conectar con {app}',
  'app.pages.login.or': 'o',
  'app.pages.login.createAnAccount': 'Crear una cuenta',
  'app.pages.login.copyright': 'Copyright © Pyplan {year}.'
}

export default messages
