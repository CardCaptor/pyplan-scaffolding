import qs from 'qs'
import { PathLike } from 'fs'
import { AxiosRequestConfig } from 'axios'

export const apiConfig: AxiosRequestConfig = {
  withCredentials: false,
  timeout: 30000,
  baseURL: process.env.REACT_APP_API_ENDPOINT || 'http://127.0.0.1:8000/api',
  headers: {
    common: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      Accept: 'application/json, text/javascript, */*; q=0.01'
    }
  },
  paramsSerializer: (params: PathLike) => qs.stringify(params, { indices: false })
}
