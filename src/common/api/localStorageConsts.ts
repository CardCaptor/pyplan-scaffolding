export const TOKEN = 'token'
export const SESSION_KEY = 'sessionKey'
export const LAST_COMPANY_SELECTED = 'lastCompanySelected'
export const LOCALE = 'locale'
