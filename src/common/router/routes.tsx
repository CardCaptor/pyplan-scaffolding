import { FC, lazy } from 'react'
import { Route, Switch } from 'react-router-dom'

const PrivateRoute = lazy(() => import('./PrivateRoute'))
const AppPage = lazy(() => import('../../pages/app'))
const LoginPage = lazy(() => import('../../pages/login'))
const NotFoundPage = lazy(() => import('../../pages/notfound'))
const SharedPage = lazy(() => import('../../pages/shared'))

const Routes: FC = () => (
  <Switch>
    <Route path='/auth/' component={LoginPage} />
    <Route path='/shared/' component={SharedPage} />
    <PrivateRoute path='/' component={AppPage} />
    <Route component={NotFoundPage} />
  </Switch>
)

export default Routes
