import { ComponentType, useEffect, useState } from 'react'
import { Route, Redirect } from 'react-router-dom'

import { securityApi } from '../../services/security'

interface Props {
  component: ComponentType<any>
  path: string
  rest?: any
  exact?: boolean
}

// A wrapper for <Route> that redirects to the login
// screen if you're not yet authenticated.
const PrivateRoute = ({ component: Component, path, exact = false, ...rest }: Props) => {
  const [loading, setLoading] = useState<boolean>(true)
  const [loggedIn, setLoggedIn] = useState<boolean>(false)
  useEffect(() => {
    const fx = async () => {
      const res = await securityApi.loggedIn()
      setLoggedIn(res)
      setLoading(false)
    }
    fx()
  }, [])
  return loading ? (
    <h1>loading</h1>
  ) : (
    <Route
      {...rest}
      path={path}
      exact={exact}
      render={(props) =>
        loggedIn ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/auth/',
              state: { from: props.location }
            }}
          />
        )
      }
    />
  )
}

export default PrivateRoute
